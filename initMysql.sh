#!/bin/sh

sleep 30

mysql --password=$MYSQL_ROOT_PASSWORD --execute "create database users_db"

mysql --password=$MYSQL_ROOT_PASSWORD --execute "CREATE TABLE users_db.user (  id BIGINT(20) NOT NULL AUTO_INCREMENT, first_name VARCHAR(45) NULL,  last_name VARCHAR(45) NULL, email VARCHAR(45) NULL, date_created DATETIME NULL, status VARCHAR(45) NOT NULL, password VARCHAR(64) NOT NULL, PRIMARY KEY (id),  UNIQUE INDEX email_UNIQUE (email ASC) );"

