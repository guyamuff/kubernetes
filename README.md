# EKS
## Start Cluster
```bash
eksctl create cluster --name cluster1 --region us-west-1 --nodes=2 --version 1.24 --managed --node-type t2.small --node-volume-size 10
```
## Enable Volume Auto-Provisioning
```bash
eksctl utils associate-iam-oidc-provider --cluster=cluster1 --approve

eksctl create iamserviceaccount  --name ebs-csi-controller-sa --namespace kube-system --cluster cluster1 --attach-policy-arn arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy --approve --role-only --role-name AmazonEKS_EBS_CSI_DriverRole

eksctl create addon --name aws-ebs-csi-driver --cluster cluster1 --service-account-role-arn arn:aws:iam::161648078964:role/AmazonEKS_EBS_CSI_DriverRole
```
## Delete Cluster
```bash
eksctl delete cluster --name cluster1 --region us-west-1
```
# Deploy Golang Project
## AWS Volumes
```bash
kubectl create -f storage.yml
```
## Configuration
```bash
kubectl create configmap init-cass --from-file=initCass.sh
kubectl create configmap init-mysql --from-file=initMysql.sh
kubectl create -f ping_secret.yml
```
## Apps and Services
```bash
kubectl create -f bookstore_oauth_deploy.yml
kubectl create -f bookstore_users_deploy.yml
```
