#!/bin/sh

sleep 30

cqlsh -e "create keyspace oauth with replication = {'class':'SimpleStrategy', 'replication_factor':1};"

cqlsh -e "create table oauth.access_token(id varchar primary key, userid bigint, clientid bigint, expires bigint);"

